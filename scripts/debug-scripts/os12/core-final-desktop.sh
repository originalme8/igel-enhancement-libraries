#!/bin/bash
# Debug Log Collection - Desktop Final Command - Version 12-20231004-001

dpart="/debuglog"

if [ -d "$dpart" ]
then
    logger -t  "igel_debug_core" "Setting up debug log"
else   
    logger -t "igel_debug_core" "No debug partition found...are you sure you checked the box?"
    exit 2
fi

bdir=$dpart/boot
hwDir=$bdir/hwinfo
netDir=$bdir/network
certDir=$bdir/certs

rm -rf "$bdir"
mkdir -p $hwDir
mkdir -p $netDir
mkdir $certDir

echo "BootTime $(date)" > $bdir/boottime.txt

echo "OS Detals" > $bdir/osdetails.txt
echo "----------------" >> $bdir/osdetails.txt
cat /etc/os-release >> $bdir/osdetails.txt
echo "" >> $bdir/osdetails.txt
echo "IGEL Detals" >> $bdir/osdetails.txt
echo "----------------" >> $bdir/osdetails.txt
echo "Product Version: $(get product.version)" >> $bdir/osdetails.txt
echo "UnitID: $(get_unit_id)" >> $bdir/osdetails.txt
echo "Hardware Address: $(getmyhwaddr)" >> $bdir/osdetails.txt

dmesg > $hwDir/dmesg.txt

cat /proc/bus/input/devices > $hwDir/inputdevices.txt
cat /proc/bus/input/devices |grep -i name > $hwDir/inputdevice-names.txt	
xinput list  > $hwDir/xinput.txt	                                        

lshw > $hwDir/lshw.txt		
lspci > $hwDir/lspci.txt	
lsusb > $hwDir/lsusb.txt

glxinfo > $hwDir/glxinfo.txt
xrandr > $hwDir/xrandr.txt
df -H > $hwDir/DiskUsage.txt

ip a > $netDir/ip.txt
ip route > $netDir/route.txt
lshw -class network > $netDir/NetworkAdapters.txt
chronyc sources > $netDir/chrony.sources
chronyc tracking > $netDir/chrony.status

unset conServers conPorts
declare -a conServers
declare -a conPorts

srvcount=$(numinstances system.remotemanager.connector%)

a=0
while [ ${srvcount} -gt 0 ]
do
    conServers+=("$(get system.remotemanager.connector${a}.address)")
    conPorts+=("$(get system.remotemanager.connector${a}.port)")
    ((srvcount--))
    ((a++))
done

pcount=0
for ums in ${conServers[@]};
do
    port=${conPorts[${pcount}]}
    unset umsTrue icgTrue
    umsTrue="$(curl -k --connect-timeout 3 https://${ums}:${port}/ums/check-status| /usr/bin/jq -r '.status')"
    icgTrue="$(curl -k --connect-timeout 3 https://${ums}:${port}/usg/check-status| /usr/bin/jq -r '.status')"

    if [ "$umsTrue" == "ok" ]
    then
        echo "- UMS Server Server [CONNECTION VALID]: ${ums}:${port}" >> $bdir/connection-servers.txt
        openssl s_client -showcerts -connect ${ums}:${port} </dev/null 2>/dev/null|openssl x509 -outform PEM >${certDir}/${ums}.pem
    elif [ "$icgTrue" == "ok" ]
    then
        echo "- ICG Server Server [CONNECTION VALID]: ${ums}:${port}" >> $bdir/connection-servers.txt
        openssl s_client -showcerts -connect ${ums}:${port} </dev/null 2>/dev/null|openssl x509 -outform PEM >${certDir}/${ums}.pem

    else
        echo "- Unable to connect to connection server: ${ums}:${port}"
    fi
    ((pcount++))
done